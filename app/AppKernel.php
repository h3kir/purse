<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 3:30 PM
 */

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AppKernel
{
    private $container;
    private $rootDir;

    public function __construct()
    {
        $this->initializeContainer();
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }

    protected function initializeContainer()
    {
        $this->container = new ContainerBuilder(new ParameterBag($this->getKernelParameters()));

        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__));
        $loader->load('config/config.yml');
    }

    /**
     * @return array
     */
    protected function getKernelParameters()
    {
        return array(
            'kernel.root_dir' => realpath($this->getRootDir()) ? : $this->getRootDir(),
            'kernel.cache_dir' => realpath($this->getCacheDir()) ? : $this->getCacheDir(),
        );
    }

    /**
     * @return mixed
     */
    public function getRootDir()
    {
        if (null === $this->rootDir) {
            $r = new \ReflectionObject($this);
            $this->rootDir = str_replace('\\', '/', dirname($r->getFileName()));
        }

        return $this->rootDir;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->getRootDir() . '/cache';
    }
} 