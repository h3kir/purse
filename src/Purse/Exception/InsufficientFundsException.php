<?php
/**
 * User: h3kir
 * Date: 10/26/16
 * Time: 12:27 AM
 */
 

namespace Purse\Exception;


/**
 * Class InsufficientFundsException
 * @package Purse\Exception
 */
class InsufficientFundsException extends \InvalidArgumentException
{

} 