<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 11:49 PM
 */
 

namespace Purse\Exception;


/**
 * Class UnsupportedCurrencyException
 * @package Purse\Exception
 */
class UnsupportedCurrencyException extends \InvalidArgumentException
{
    private $currency;

    /**
     * @param string $currency
     */
    public function __construct($currency)
    {
        parent::__construct(sprintf('Currency "%s" is not supported.'));

        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
} 