<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 12:17 PM
 */

namespace Purse\Command;


use Purse\Exception\ParserException;
use Purse\Exception\UnsupportedCurrencyException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

use Purse\Purse;

/**
 * Class PutMoneyCommand
 * @package Purse\Command
 */
class PutMoneyCommand extends Command
{
    private $purse;

    /**
     * @param Purse $purse
     */
    public function __construct(Purse $purse)
    {
        $this->purse = $purse;

        parent::__construct();
    }

    /**
     * @return Purse
     */
    protected function getPurse()
    {
        return $this->purse;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('purse:put')
            ->setDescription('Пополнить счет')
            ->addArgument('amount', null, 'Сумма пополения')
            ->addArgument('currency', null, 'Валюта пополнения');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $amount = $input->getArgument('amount');
        $currency = $input->getArgument('currency');

        try{
            $resultBalance = $this->getPurse()->put($amount, $currency);
            $output->writeln(sprintf('На счете: %s %s', $resultBalance->format(), $resultBalance->getCurrency()));
        }catch (ParserException $ex){
            $output->writeln('Не удалось распознать число, укажите значение вида 1.00');
        }catch (UnsupportedCurrencyException $ex){
            $output->writeln(sprintf('Валюта "%s" не поддреживается', $ex->getCurrency()));
        }catch (\Exception $ex){
            $output->writeln('Ошибка во время выполнения:');
            $output->writeln($ex->getMessage());
        }
    }
} 