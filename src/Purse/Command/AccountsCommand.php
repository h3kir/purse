<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 12:17 PM
 */

namespace Purse\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

use Purse\Purse;

/**
 * Class AccountsCommand
 * @package Purse\Command
 */
class AccountsCommand extends Command
{
    private $purse;

    public function __construct(Purse $purse)
    {
        $this->purse = $purse;

        parent::__construct();
    }

    /**
     * @return Purse
     */
    protected function getPurse()
    {
        return $this->purse;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('purse:accounts')
            ->setDescription('Информация по всем счетам');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = new Table($output);
        $table
            ->setHeaders(array('Валюта', 'Сумма'));

        foreach ($this->getPurse()->allAccountBalances() as $accountBalance) {
            /** @var \Purse\Money\MoneyInterface */
            $table->addRow([
                $accountBalance->getCurrency(),
                $accountBalance->format(),
            ]);
        }

        $table->render();
    }
} 