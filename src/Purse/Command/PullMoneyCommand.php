<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 12:17 PM
 */

namespace Purse\Command;

use Purse\Exception\InsufficientFundsException;
use Purse\Exception\ParserException;
use Purse\Exception\UnsupportedCurrencyException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

use Purse\Purse;

/**
 * Class PullMoneyCommand
 * @package Purse\Command
 */
class PullMoneyCommand extends Command
{
    private $purse;

    /**
     * @param Purse $purse
     */
    public function __construct(Purse $purse)
    {
        $this->purse = $purse;

        parent::__construct();
    }

    /**
     * @return Purse
     */
    protected function getPurse()
    {
        return $this->purse;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('purse:pull')
            ->setDescription('Снять со счета')
            ->addArgument('amount', null, 'Сумма снятия')
            ->addArgument('currency', null, 'Валюта снятия');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $amount = $input->getArgument('amount');
        $currency = $input->getArgument('currency');

        try{
            $resultBalance = $this->getPurse()->pull($amount, $currency);
            $output->writeln(sprintf('На счете: %s %s', $resultBalance->format(), $resultBalance->getCurrency()));
        }catch (ParserException $ex){
            $output->writeln('Не удалось распознать число, укажите значение вида 1.00');
        }catch (UnsupportedCurrencyException $ex){
            $output->writeln(sprintf('Валюта "%s" не поддреживается.', $ex->getCurrency()));
        }catch (InsufficientFundsException $ex){
            $output->writeln('Недостаточно средств.');
        }catch (\Exception $ex){
            $output->writeln('Ошибка во время выполнения:');
            $output->writeln($ex->getMessage());
        }
    }
} 