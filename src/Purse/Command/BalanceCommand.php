<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 12:17 PM
 */

namespace Purse\Command;

use Purse\Exception\ConverterException;
use Purse\Exception\UnsupportedCurrencyException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

use Purse\Purse;

/**
 * Class BalanceCommand
 * @package Purse\Command
 */
class BalanceCommand extends Command
{
    private $purse;

    /**
     * @param Purse $purse
     */
    public function __construct(Purse $purse)
    {
        $this->purse = $purse;

        parent::__construct();
    }

    /**
     * @return Purse
     */
    protected function getPurse()
    {
        return $this->purse;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('purse:balance')
            ->setDescription('Общая сумма')
            ->addArgument('currency', null, 'Валюта конвертации', 'RUB');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currency = $input->getArgument('currency');

        try{
            $balance = $this->getPurse()->balance($currency);
            $output->writeln(sprintf('Итого: %s %s', $balance->format(), $balance->getCurrency()));
        }catch (ConverterException $ex){
            $output->writeln('Ошибка конвертации:');
            $output->writeln($ex->getMessage());
        }catch (UnsupportedCurrencyException $ex){
            $output->writeln(sprintf('Валюта "%s" не поддреживается', $ex->getCurrency()));
        }catch (\Exception $ex){
            $output->writeln('Ошибка во время выполнения:');
            $output->writeln($ex->getMessage());
        }
    }
} 