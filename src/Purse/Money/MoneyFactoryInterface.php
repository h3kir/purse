<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 3:31 PM
 */
 

namespace Purse\Money;


/**
 * Interface MoneyFactoryInterface
 * @package Purse\Money
 */
interface MoneyFactoryInterface
{
    /**
     * @param $amount
     * @param $currency
     * @return MoneyInterface
     */
    public function create($amount, $currency);
} 