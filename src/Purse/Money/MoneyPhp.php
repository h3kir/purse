<?php
/**
 * User: h3kir
 * Date: 10/23/16
 * Time: 8:27 PM
 */


namespace Purse\Money;

use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Currencies\ISOCurrencies;
use Money\Money;

/**
 * Class MoneyPhp
 * @package Purse\Money
 */
class MoneyPhp implements MoneyInterface
{
    private $money;

    /**
     * @param $amount
     * @param $currency
     */
    public function __construct($amount, $currency)
    {
        $this->money = new Money($amount, new Currency($currency));
    }

    /**
     * @return Money
     */
    protected function getMoney()
    {
        return $this->money;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->money->getCurrency()->getCode();
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->money->getAmount();
    }

    /**
     * @return string
     */
    public function format()
    {
        $currencies = new ISOCurrencies();

        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        return $moneyFormatter->format($this->money); // outputs 1.00
    }

    /**
     * @param MoneyInterface $addend
     * @return MoneyInterface
     */
    public function add(MoneyInterface $addend)
    {
        $addendMoney = new Money($addend->getAmount(), new Currency($addend->getCurrency()));
        $newMoney = $this->money->add($addendMoney);

        return new self($newMoney->getAmount(), $this->getCurrency());
    }

    /**
     * @param MoneyInterface $subtracted
     * @return MoneyInterface
     */
    public function subtract(MoneyInterface $subtracted)
    {
        $subtractedMoney = new Money($subtracted->getAmount(), new Currency($subtracted->getCurrency()));
        $newMoney = $this->money->subtract($subtractedMoney);

        return new self($newMoney->getAmount(), $this->getCurrency());
    }

    /**
     * @param MoneyInterface $other
     * @return int|mixed
     */
    public function compare(MoneyInterface $other)
    {
        $otherMoney = new Money($other->getAmount(), new Currency($other->getCurrency()));

        return $this->money->compare($otherMoney);
    }

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function greaterThan(MoneyInterface $other)
    {
        $otherMoney = new Money($other->getAmount(), new Currency($other->getCurrency()));

        return $this->money->greaterThan($otherMoney);
    }

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function greaterThanOrEqual(MoneyInterface $other)
    {
        $otherMoney = new Money($other->getAmount(), new Currency($other->getCurrency()));

        return $this->money->greaterThanOrEqual($otherMoney);
    }

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function lessThan(MoneyInterface $other)
    {
        $otherMoney = new Money($other->getAmount(), new Currency($other->getCurrency()));

        return $this->money->lessThan($otherMoney);
    }

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function lessThanOrEqual(MoneyInterface $other)
    {
        $otherMoney = new Money($other->getAmount(), new Currency($other->getCurrency()));

        return $this->money->lessThanOrEqual($otherMoney);
    }
} 