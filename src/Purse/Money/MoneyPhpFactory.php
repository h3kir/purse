<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 3:32 PM
 */
 

namespace Purse\Money;

/**
 * Class MoneyPhpFactory
 * @package Purse\Money
 */
class MoneyPhpFactory implements MoneyFactoryInterface
{
    /**
     * @param $amount
     * @param $currency
     * @return MoneyInterface
     */
    public function create($amount, $currency)
    {
        return new MoneyPhp($amount, $currency);
    }
} 