<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 3:00 PM
 */
 

namespace Purse\Money;


/**
 * Interface MoneyInterface
 * @package Purse\Money
 */
interface MoneyInterface
{
    /**
     * @return integer
     */
    public function getAmount();

    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @return string
     */
    public function format();

    /**
     * @param MoneyInterface $addend
     * @return MoneyInterface
     */
    public function add(MoneyInterface $addend);

    /**
     * @param MoneyInterface $subtracted
     * @return MoneyInterface
     */
    public function subtract(MoneyInterface $subtracted);

    /**
     * @param MoneyInterface $other
     * @return mixed
     */
    public function compare(MoneyInterface $other);

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function greaterThan(MoneyInterface $other);

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function greaterThanOrEqual(MoneyInterface $other);

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function lessThan(MoneyInterface $other);

    /**
     * @param MoneyInterface $other
     * @return bool
     */
    public function lessThanOrEqual(MoneyInterface $other);
}