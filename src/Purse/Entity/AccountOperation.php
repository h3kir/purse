<?php
/**
 * User: h3kir
 * Date: 10/23/16
 * Time: 10:57 PM
 */


namespace Purse\Entity;


/**
 * @Entity(repositoryClass="Purse\Repository\AccountOperationRepository")
 * @Table(name="account_operation")
 * @HasLifecycleCallbacks
 */
class AccountOperation
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="integer", name="account_id")
     */
    protected $accountId;

    /**
     * @Column(type="string")
     */
    protected $currency;

    /**
     * @Column(type="integer")
     */
    protected $amount;

    /**
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }
} 