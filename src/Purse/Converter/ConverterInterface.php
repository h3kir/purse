<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 3:15 PM
 */
 

namespace Purse\Converter;


use Purse\Money\MoneyInterface;

/**
 * Interface ConverterInterface
 * @package Purse\Converter
 */
interface ConverterInterface
{
    /**
     * @param MoneyInterface $money
     * @param $currency
     * @return mixed
     */
    public function convert(MoneyInterface $money, $currency);
} 