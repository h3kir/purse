<?php
/**
 * User: h3kir
 * Date: 10/23/16
 * Time: 11:56 PM
 */


namespace Purse\Converter;

use Money\Converter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exchange\SwapExchange;
use Money\Money;
use Swap\Builder;
use Swap\Service\Registry;

use Purse\Money\MoneyFactoryInterface;
use Purse\Money\MoneyInterface;
use Purse\Exception\ConverterException;

/**
 * Class MoneyPhpConverter
 * @package Purse\Converter
 */
class MoneyPhpConverter implements ConverterInterface
{
    private $converter;
    private $moneyFactory;

    /**
     * @param MoneyFactoryInterface $moneyFactory
     */
    public function __construct(MoneyFactoryInterface $moneyFactory)
    {
        $this->moneyFactory = $moneyFactory;

        Registry::register('cbr', CentralBankOfRussianFederation::class);

        $swap = (new Builder())
            ->add('cbr')
            ->build();

        $exchanger = new SwapExchange($swap);

        $this->converter = new Converter(new ISOCurrencies(), $exchanger);
    }

    /**
     * @param MoneyInterface $money
     * @param $currency
     * @return MoneyInterface
     * @throws \Purse\Exception\ConverterException
     */
    public function convert(MoneyInterface $money, $currency)
    {
        try{
            $moneyObj = new Money($money->getAmount(), new Currency($money->getCurrency()));
            $convertedMoney = $this->getConverter()->convert($moneyObj, new Currency($currency));

            $moneyPhp = $this->getMoneyFactory()->create(
                $convertedMoney->getAmount(),
                $convertedMoney->getCurrency()->getCode()
            );

            return $moneyPhp;
        }catch (\Exception $ex){
            throw new ConverterException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    /**
     * @return Converter
     */
    protected function getConverter()
    {
        return $this->converter;
    }

    /**
     * @return MoneyFactoryInterface
     */
    protected function getMoneyFactory()
    {
        return $this->moneyFactory;
    }
} 