<?php
/**
 * User: h3kir
 * Date: 10/24/16
 * Time: 9:31 AM
 */


namespace Purse\Converter;

use Exchanger\Exception\UnsupportedCurrencyPairException;
use Exchanger\HistoricalExchangeRateQuery;
use Exchanger\Service\Service;
use Exchanger\Contract\ExchangeRateQuery;
use Exchanger\ExchangeRate;
use Exchanger\StringUtil;

/**
 * Class CentralBankOfRussianFederation
 * @package Purse\Converter
 */
class CentralBankOfRussianFederation extends Service
{
    const DAILY_URL = 'http://cbr.ru/scripts/XML_daily.asp';
    const RUB = 'RUB';

    /**
     * @param ExchangeRateQuery $exchangeQuery
     * @return \Exchanger\Contract\ExchangeRate|ExchangeRate
     * @throws \Exchanger\Exception\UnsupportedCurrencyPairException
     */
    public function getExchangeRate(ExchangeRateQuery $exchangeQuery)
    {
        $content = $this->request(self::DAILY_URL);

        $element = StringUtil::xmlToElement($content);

        $baseCurrency = $exchangeQuery->getCurrencyPair()->getBaseCurrency();
        $baseValue = 1;
        $baseNominal = 1;

        /**
         * Если базовая валюта не рубль находим ее курс к рублю
         */
        if ($baseCurrency != self::RUB) {
            $elements = $element->xpath('//Valute[child::CharCode[text()="' . $baseCurrency . '"]]');

            if (empty($elements)) {
                throw new UnsupportedCurrencyPairException($exchangeQuery->getCurrencyPair(), $this);
            }

            $baseValue = (float)str_replace(',', '.', (string)$elements[0]->Value);
            $baseNominal = (int)$elements[0]->Nominal;
        }

        $quoteCurrency = $exchangeQuery->getCurrencyPair()->getQuoteCurrency();
        $quoteValue = 1;
        $quoteNominal = 1;

        /**
         * Если валюта конвертации не рубль находим ее курс к рублю
         */
        if ($quoteCurrency != self::RUB) {
            $elements = $element->xpath('//Valute[child::CharCode[text()="' . $quoteCurrency . '"]]');

            if (empty($elements)) {
                throw new UnsupportedCurrencyPairException($exchangeQuery->getCurrencyPair(), $this);
            }

            $quoteValue = (float)str_replace(',', '.', (string)$elements[0]->Value);
            $quoteNominal = (int)$elements[0]->Nominal;
        }

        /**
         * По этой формуле находим курс базовой вылюты к валюте конвертации
         */
        $value = ($baseValue * $quoteNominal) / ($baseNominal * $quoteValue);

        $date = new \DateTime((string)$element->xpath('//ValCurs/@Date')[0]);

        return new ExchangeRate($value, $date);
    }

    /**
     * @param ExchangeRateQuery $exchangeQuery
     *
     * @return bool
     */
    public function supportQuery(ExchangeRateQuery $exchangeQuery)
    {
        return !$exchangeQuery instanceof HistoricalExchangeRateQuery;
    }
}