<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 11:16 PM
 */
 

namespace Purse\Parser;


use Money\Currencies\ISOCurrencies;
use Money\Exception\ParserException;
use Money\Parser\DecimalMoneyParser;

use Purse\Money\MoneyFactoryInterface;

/**
 * Class MoneyPhpParser
 * @package Purse\Parser
 */
class MoneyPhpParser implements ParserInterface
{
    private $moneyFactory;

    public function __construct(MoneyFactoryInterface $moneyFactory)
    {
        $this->moneyFactory = $moneyFactory;
    }

    /**
     * @param string $amount
     * @param string $currency
     * @return \Purse\Money\MoneyInterface
     * @throws \Purse\Exception\ParserException
     */
    public function parse($amount, $currency)
    {
        $parser = new DecimalMoneyParser(new ISOCurrencies());

        try{
            /** @var \Money\Money $money */
            $money = $parser->parse($amount, $currency);
            $moneyPhp = $this->getMoneyFactory()->create($money->getAmount(), $money->getCurrency()->getCode());

            return $moneyPhp;
        }catch (ParserException $ex){
            throw new \Purse\Exception\ParserException;
        }
    }

    /**
     * @return MoneyFactoryInterface
     */
    protected function getMoneyFactory()
    {
        return $this->moneyFactory;
    }
} 