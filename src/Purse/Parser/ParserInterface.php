<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 11:15 PM
 */
 

namespace Purse\Parser;


/**
 * Interface ParserInterface
 * @package Purse\Parser
 */
interface ParserInterface
{
    /**
     * @param string $amount
     * @param string $currency
     * @return \Purse\Money\MoneyInterface
     */
    public function parse($amount, $currency);
} 