<?php
/**
 * User: h3kir
 * Date: 10/21/16
 * Time: 2:11 PM
 */


namespace Purse\Doctrine;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * Class Doctrine
 * @package Purse\Doctrine
 */
class Doctrine
{
    private $em;
    private $driver;
    private $host;
    private $port;
    private $dbName;
    private $user;
    private $password;
    private $charset;
    private $cache;
    private $proxyPath;

    /**
     * @param $driver
     * @param $host
     * @param $port
     * @param $dbName
     * @param $user
     * @param $password
     * @param $charset
     */
    public function __construct($driver, $host, $port, $dbName, $user, $password, $charset)
    {
        $this->driver = $driver;
        $this->host = $host;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->password = $password;
        $this->charset = $charset;
    }

    /**
     * @return EntityManager
     */
    protected function initDoctrine()
    {
        $cache = $this->getCache();

        $isDev = false;
        $config = Setup::createAnnotationMetadataConfiguration(
            array(__DIR__ . "/../"),
            $isDev,
    null,
            $cache
        );

        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir($this->getProxyPath());
        $config->setProxyNamespace('Proxies');
        $config->setAutoGenerateProxyClasses(true);

        $connectionOptions = array(
            'driver' => $this->getDriver(),
            'user' => $this->getUser(),
            'password' => $this->getPassword(),
            'host' => $this->getHost(),
            'dbname' => $this->getDbname(),
            'charset' => $this->getCharset(),
            'port' => $this->getPort(),
        );

        return EntityManager::create($connectionOptions, $config);
    }

    public function getManager()
    {
        if (!$this->em) {
            $this->em = $this->initDoctrine();
        }

        return $this->em;
    }

    /**
     * @param mixed $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param mixed $driver
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @return mixed
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param mixed $cache
     */
    public function setCache(\Doctrine\Common\Cache\CacheProvider $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return mixed
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $proxyPath
     */
    public function setProxyPath($proxyPath)
    {
        $this->proxyPath = $proxyPath;
    }

    /**
     * @return mixed
     */
    public function getProxyPath()
    {
        return $this->proxyPath;
    }
}