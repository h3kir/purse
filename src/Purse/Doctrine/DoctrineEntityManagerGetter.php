<?php
/**
 * User: h3kir
 * Date: 10/25/16
 * Time: 12:00 AM
 */


namespace Purse\Doctrine;

/**
 * Class DoctrineEntityManagerGetter
 * @package Purse\Doctrine
 */
class DoctrineEntityManagerGetter
{
    /**
     * @param Doctrine $doctrine
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getManager(Doctrine $doctrine)
    {
        return $doctrine->getManager();
    }
} 