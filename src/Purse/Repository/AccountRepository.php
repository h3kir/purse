<?php
/**
 * User: h3kir
 * Date: 10/22/16
 * Time: 11:50 PM
 */


namespace Purse\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AccountRepository
 * @package Purse\Repository
 */
class AccountRepository extends EntityRepository
{
    public function findByCurrency($currencyCode)
    {
        return $this->findOneBy(['currency' => $currencyCode]);
    }
} 