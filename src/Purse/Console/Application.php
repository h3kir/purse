<?php
/**
 * User: h3kir
 * Date: 10/20/16
 * Time: 12:27 PM
 */


namespace Purse\Console;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Purse\Command\Command;

/**
 * Class Application
 * @package Purse\Console
 */
class Application extends BaseApplication
{
    private $container;
    private $commandsRegistered = false;

    /**
     * @param ContainerBuilder $container
     */
    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function doRun(InputInterface $input, OutputInterface $output)
    {
        if (!$this->commandsRegistered) {
            $this->registerCommands($output);
            $this->commandsRegistered = true;
        }

        return parent::doRun($input, $output);
    }

    /**
     *
     */
    protected function registerCommands()
    {
        $this->registerTaggedServiceIds();
    }

    /**
     * @throws \Exception
     */
    protected function registerTaggedServiceIds()
    {
        $serviceIds = array_keys($this->getContainer()->findTaggedServiceIds('console.command'));
        foreach ($serviceIds as $id) {
            $command = $this->getContainer()->get($id);
            if ($command instanceof Command) {
                $this->add($command);
            } else {
                throw new \Exception("Service \"{$id}\" not instanceof Purse\\Command\\Command class");
            }
        }
    }
} 