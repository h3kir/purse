<?php
/**
 * User: h3kir
 * Date: 10/23/16
 * Time: 6:56 PM
 */


namespace Purse;


use Doctrine\ORM\EntityManager;
use Purse\Entity\Account;
use Purse\Entity\AccountOperation;
use Purse\Exception\InsufficientFundsException;
use Purse\Exception\UnsupportedCurrencyException;
use Purse\Money\MoneyFactoryInterface;
use Purse\Money\MoneyInterface;
use Purse\Converter\ConverterInterface;
use Purse\Parser\ParserInterface;

/**
 * Class Purse
 * @package Purse\Model
 */
class Purse
{
    private $em;
    private $moneyFactory;
    private $converter;
    private $parser;
    private $supportedCurrencies = [
        'RUB',
        'USD',
        'EUR',
        'KGS',
    ];

    /**
     * @param EntityManager $em
     * @param MoneyFactoryInterface $moneyFactory
     * @param ConverterInterface $converter
     * @param ParserInterface $parser
     */
    public function __construct(
        EntityManager $em,
        MoneyFactoryInterface $moneyFactory,
        ConverterInterface $converter,
        ParserInterface $parser
    )
    {
        $this->em = $em;
        $this->moneyFactory = $moneyFactory;
        $this->converter = $converter;
        $this->parser = $parser;
    }

    /**
     * @param $amount
     * @param $currency
     * @return MoneyInterface
     */
    public function put($amount, $currency)
    {
        $money = $this->parseMoney($amount, $currency);

        return $this->putMoney($money);
    }

    /**
     * @param MoneyInterface $money
     * @return MoneyInterface
     * @throws Exception\UnsupportedCurrencyException
     * @throws \Exception
     */
    public function putMoney(MoneyInterface $money)
    {
        if(!$this->validateCurrency($money->getCurrency())){
            throw new UnsupportedCurrencyException($money->getCurrency());
        }

        $account = $this->getOrCreateIfNotExistsAccount($money->getCurrency());
        $accountBalance = $this->getAccountBalance($account);
        $result = $accountBalance->add($money);

        $this->getManager()->beginTransaction();
        try {
            $operation = new AccountOperation();
            $operation->setAmount($money->getAmount());
            $operation->setCurrency($money->getCurrency());
            $operation->setAccountId($account->getId());
            $this->getManager()->persist($operation);

            $account->setAmount($result->getAmount());
            $this->getManager()->persist($account);

            $this->getManager()->flush();
            $this->getManager()->commit();
        } catch (\Exception $ex) {
            $this->getManager()->rollback();

            throw $ex;
        }

        return $this->getAccountBalance($account);
    }

    /**
     * @param $amount
     * @param $currency
     * @return MoneyInterface
     */
    public function pull($amount, $currency)
    {
        $money = $this->parseMoney($amount, $currency);

        return $this->pullMoney($money);
    }

    /**
     * @param MoneyInterface $money
     * @return MoneyInterface
     * @throws Exception\InsufficientFundsException
     * @throws Exception\UnsupportedCurrencyException
     * @throws \Exception
     */
    public function pullMoney(MoneyInterface $money)
    {
        if(!$this->validateCurrency($money->getCurrency())){
            throw new UnsupportedCurrencyException($money->getCurrency());
        }

        $currency = $money->getCurrency();
        $account = $this->getOrCreateIfNotExistsAccount($currency);
        $accountBalance = $this->getAccountBalance($account);

        if ($accountBalance->greaterThanOrEqual($money)) {
            $result = $accountBalance->subtract($money);

            $this->getManager()->beginTransaction();
            try {
                $operation = new AccountOperation();
                $operation->setAmount(-$money->getAmount());
                $operation->setCurrency($money->getCurrency());
                $operation->setAccountId($account->getId());
                $this->getManager()->persist($operation);

                $account->setAmount($result->getAmount());
                $this->getManager()->persist($account);

                $this->getManager()->flush();
                $this->getManager()->commit();
            } catch (\Exception $ex) {
                $this->getManager()->rollback();

                throw $ex;
            }
        } else {
            throw new InsufficientFundsException;
        }

        return $this->getAccountBalance($account);
    }

    /**
     * @param $currency
     * @return MoneyInterface
     * @throws \Purse\Exception\UnsupportedCurrencyException
     */
    public function balance($currency)
    {
        if(!$this->validateCurrency($currency)){
            throw new UnsupportedCurrencyException($currency);
        }

        $total = $this->getMoneyFactory()->create(0, $currency);
        foreach ($this->getAccounts() as $account) {
            $accountBalance = $this->getAccountBalance($account);
            $convertedAccountBalance = $this->converter->convert($accountBalance, $currency);
            $total = $total->add($convertedAccountBalance);
        }

        return $total;
    }

    /**
     * @return MoneyInterface[]
     */
    public function allAccountBalances()
    {
        $result = [];

        foreach ($this->getAccounts() as $account) {
            $result [] = $this->getAccountBalance($account);
        }

        return $result;
    }

    /**
     * @param $currency
     * @return bool
     */
    public function validateCurrency($currency)
    {
        return in_array($currency, $this->getSupportedCurrencies());
    }

    /**
     * @param array $supportedCurrencies
     */
    public function setSupportedCurrencies($supportedCurrencies)
    {
        $this->supportedCurrencies = $supportedCurrencies;
    }

    /**
     * @return array
     */
    public function getSupportedCurrencies()
    {
        return $this->supportedCurrencies;
    }

    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        return $this->em;
    }

    /**
     * @return MoneyFactoryInterface
     */
    protected function getMoneyFactory()
    {
        return $this->moneyFactory;
    }

    /**
     * @return ConverterInterface
     */
    protected function getConverter()
    {
        return $this->converter;
    }

    /**
     * @return ParserInterface
     */
    protected function getParser()
    {
        return $this->parser;
    }

    /**
     * @return \Purse\Repository\AccountRepository
     */
    protected function getAccountsRepo()
    {
        return $this->getManager()->getRepository('\Purse\Entity\Account');
    }

    /**
     * @param $currency
     * @return null|Account
     */
    protected function getAccountByCurrency($currency)
    {
        return $this->getAccountsRepo()->findByCurrency($currency);
    }

    /**
     * @param $currency
     * @return Account
     */
    protected function createAccount($currency)
    {
        $account = new Account();
        $account->setCurrency($currency);
        $account->setAmount(0);

        $this->getManager()->persist($account);
        $this->getManager()->flush();

        return $account;
    }

    /**
     * @param $currency
     * @return null|Account
     */
    protected function getOrCreateIfNotExistsAccount($currency)
    {
        $account = $this->getAccountByCurrency($currency);
        if (!$account) {
            $account = $this->createAccount($currency);
        }

        return $account;
    }

    /**
     * @param Account $account
     * @return MoneyInterface
     */
    protected function getAccountBalance(Account $account)
    {
        return $this->getMoneyFactory()->create($account->getAmount(), $account->getCurrency());
    }

    /**
     * @return array|\Purse\Entity\Account[]
     */
    protected function getAccounts()
    {
        return $this->getAccountsRepo()->findAll();
    }

    /**
     * @param $amount
     * @param $currency
     * @return MoneyInterface
     */
    protected function parseMoney($amount, $currency)
    {
        return $this->getParser()->parse($amount, $currency);
    }
}