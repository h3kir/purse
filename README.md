#Кошелек (тестовое задание)

##Инструкция по установке
composer install

app/doctrine orm:schema-tool:create

##Инструкция по применению
app/console purse:accounts - информация по всем счетам в кошельке

app/console purse:balance [currency] - баланс кошелька в указанной валюте

app/console purse:put [amount] [currency] - положить в кошелек

app/console purse:pull [amount] [currency] - снять с кошелька

##Комментарии
Коды валют используются по стандарту ISO 4217

Счета создаются при первом пополнении